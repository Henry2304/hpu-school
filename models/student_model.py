from odoo import models, fields, api
from odoo.exceptions import ValidationError

class SaleOderInherit(models.Model):
    _inherit = 'sale.order'

    name_student = fields.Char(string='Name', required=True)


class Students(models.Model):
    _name = 'student.hpu'
    _description = 'Students'
    _rec_name = 'name_student'

    _inherit = ['mail.thread.cc', 'mail.activity.mixin']
    name_student = fields.Char(string='Name', required=True)
    student_age = fields.Integer(string='Age')
    image = fields.Binary(string='Image', attachment=True)
    name_subject_id = fields.Many2many('subjects.hpu', string="Subjects", ondelete="cascade")
    notes = fields.Text(string="Notes")

    @api.depends('student_age')
    def _set_age_group(self):
        if self.student_age:
            if 6 <= self.student_age <= 10:
                self.age_group = 'primary'
            elif 11 <= self.student_age <= 14:
                self.age_group = 'junior'
            elif 15 <= self.student_age <= 17:
                self.age_group = 'highschool'

    gender = fields.Selection([
        ('male', 'Male'),
        ('fe_male', 'Female'),
        ('other', 'Other'),
        ], default='male', string='Gender')
    age_group = fields.Selection([
        ('primary', 'Primary School'),
        ('junior', 'Junior High School'),
        ('highschool', 'High School')], string='Education', compute='_set_age_group')
    class_room = fields.Char(string='Class')

    student_seq = fields.Char(string='ID', required=True, copy=False, readonly=True,
                              index=True, default=lambda self: 'New')

    @api.model
    def create(self, vals):
        if vals.get('student_seq', 'New') == 'New':
                vals['student_seq'] = self.env['ir.sequence'].next_by_code('student.hpu.sequence') or 'New'
        result = super(Students, self).create(vals)
        return result

    @api.constrains('student_age')
    def _check_age(self):
        for n in self:
            if n.student_age < 6:
                raise ValidationError("Student age must be bigger! : %s" % n.student_age)
            elif n.student_age > 17:
                raise ValidationError("Student age must be smaller! : %s" % n.student_age)


