from odoo import api, fields, models

class Subjects(models.Model):
    _name = 'subjects.hpu'
    _description = 'Subjects'

    name_student_id = fields.Many2one('student.hpu', string="Student", ondelete="cascade")
    name_subject = fields.Char(string="Subject", required=True)
    student_age2 = fields.Integer(string="Age", related='name_student_id.student_age')
    name_teacher = fields.Char(string="Teacher")



