{
    'name': "HPU school",

    'summary': """""",

    'description': """Management school""",

    'author': "Huy Henry",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Extra Tools',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail', 'sale'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/sequence.xml',
        'views/subjects_view.xml',
        'views/student_view.xml',

    ],
    # only loaded in demonstration mode
    'installable': True,
    'auto_install': False,

}
